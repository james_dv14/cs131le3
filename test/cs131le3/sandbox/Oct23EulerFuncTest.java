/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs131le3.sandbox;

import cs131le3.core.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author js
 */
public class Oct23EulerFuncTest {
  
  Vector v;
  
  public Oct23EulerFuncTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of evaluate method, of class Oct23Euler.
   */
  @Test
  public void testEvaluate() {
    double[] arr = { 1.5, -2.5 };
    v = new Vector(arr);
    Vector result = (new Oct23EulerFunc()).evaluate(v);
    assertTrue(result.get(0) == -2.5);
    assertTrue(result.get(1) == 5.5);
  }
  
}
