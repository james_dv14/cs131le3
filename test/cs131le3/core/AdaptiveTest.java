/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs131le3.core;

import cs131le3.sandbox.Oct23EulerFunc;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author js
 */
public class AdaptiveTest {
  Vector v;
  Adaptive solver;
  double v1 = 1.5;
  double v2 = -2.5;
  
  public AdaptiveTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    double[] arr = { v1, v2 };
    v = new Vector(arr);
    solver = new Adaptive();
  }
  
  @After
  public void tearDown() {
  }

  @Test
  public void testSolve() {
    VectorGiven given = new VectorGiven(v, new Oct23EulerFunc(), 0, 0.5);
    solver.solve(given, new Euler(), 10, 0.15);
    assertTrue(solver.log.size() == 4);
    System.out.println(solver);
  }
  
}
