/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs131le3.core;

import cs131le3.sandbox.Oct23EulerFunc;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author js
 */
public class EulerTest {
  Vector v;
  Euler solver;
  double v1 = 1.5;
  double v2 = -2.5;
  
  public EulerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    double[] arr = { v1, v2 };
    v = new Vector(arr);
    solver = new Euler();
  }
  
  @After
  public void tearDown() {
  }
  
  @Test
  public void testNext() {
    Vector result = solver.next(v, new Oct23EulerFunc(), 0.1);
    assertTrue(result.get(0) == 1.25);
    assertTrue(result.get(1) == -1.95);
  }
  
  @Test
  public void testSolve() {
    VectorGiven given = new VectorGiven(v, new Oct23EulerFunc(), 0, 0.5);
    solver.solve(given, 0.1);
    
    Vector last = solver.log.get(solver.log.size() - 1);
    String truncated = String.format("%.5f", last.get(0));
    assertEquals(truncated, "0.67452");
    
    truncated = String.format("%.5f", last.get(1));
    assertEquals(truncated, "-0.84259");
    
    System.out.println(solver);
  }
  
}
