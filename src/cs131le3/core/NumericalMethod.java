/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131le3.core;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class NumericalMethod {
  
  /**
   * 
   * @param given Common given for vectors: the initial values, the vector 
   * function, and the starting and ending values for the independent variable.
   * @param h Step size.
   * @return 
   */
  public double solve(VectorGiven given, double h) {
    Vector result = new Vector(given.v);
    double max_diff = Double.MAX_VALUE;
    
    for (double i = given.a; i < given.b; i += h) {
      Vector previous = new Vector(result);
      log(result);
      result = next(result, given.f, h);
      max_diff = result.sub(previous).max();
    }
    
    log(result);
    
    return max_diff;
  }
  
  protected abstract void log(Vector result);
  
  protected abstract Vector next(Vector current, VectorFunction f, double h);

}
