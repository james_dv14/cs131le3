/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131le3.core;

import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class RK4 extends NumericalMethod {
  Vector[] k;
  ArrayList<Vector> result_log;
  ArrayList<Vector[]> k_log;
  double h;
  
  public RK4() {
    result_log = new ArrayList();
    k_log = new ArrayList();
  }
  
  @Override
  public double solve(VectorGiven given, double h) {
    this.h = h;
    return super.solve(given, h);
  }
  
  
  /**
   * x_{n+1} = x_n + h/6 * (k1 + 2*k2 + 2*k3 + k4)
   * 
   * @param current Values of the input vector at the current time step.
   * @param f A function that is the derivative of the vector.
   * @param h Step size.
   * @return The values of the input vector at the next time step in RK4 
   * method (i.e. x_{n+1}).
   */
  @Override
  protected Vector next(Vector current, VectorFunction f, double h) {
    Vector tmp = new Vector(current);
    k = new Vector[4];
    
    // f(x) = x'
    k[0] = f.evaluate(tmp); // k1 = f(x)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h / 2 * k[0].get(i));
    k[1] = f.evaluate(tmp); // k2 = f(x + h/2 * k1)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h / 2 * k[1].get(i));
    k[2] = f.evaluate(tmp); // k3 = f(x + h/2 * k2)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h * k[2].get(i));
    k[3] = f.evaluate(tmp); // k4 = f(x + h * k3)
    
    /*
      Feeling ko talaga mali si sir eh

      tmp.set(0, current.get(0) + h/2 * k[2].get(0));
      tmp.set(1, current.get(1) + h * k[2].get(1));
      tmp = f.evaluate(tmp);

      k[3].set(1, tmp.get(1));
    */
    
    k_log.add(k);
    
    tmp = k[0].add(k[1].mult(2)); // tmp = k1 + 2*k2
    tmp = tmp.add(k[2].mult(2));  // tmp = k1 + 2*k2 + 2*k3
    tmp = tmp.add(k[3]);          // tmp = k1 + 2*k2 + 2*k3 + k4
    tmp = tmp.mult(h / 6);        // tmp = (h/6) (k1 + 2*k2 + 2*k3 + k4)
    return current.add(tmp);      // return x_n + h/6 * (k1 + 2*k2 + 2*k3 + k4)
  }
  
  @Override
  protected void log(Vector result) {
    result_log.add(result);
  }
  
  @Override
  public String toString() {
    String str = "";
    
    for (int i = 0; i < result_log.size(); i++) {
      str += String.format("x(%.5f) = ", h * i);
      str += result_log.get(i);
      str += "\n";
      if (i < result_log.size() - 1) {
        for (int j = 0; j < 4; j++) {
          str += String.format("  k%d =", j);
          str += k_log.get(i)[j];
          str += "\n";
        }
      }
      str += "\n";
    }
    
    return str.trim();
  }

}
