/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131le3.core;

import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Euler extends NumericalMethod {
  ArrayList<Vector> log;
  double h;
  
  public Euler() {
    log = new ArrayList();
  }
  
  
  @Override
  public double solve(VectorGiven given, double h) {
    this.h = h;
    return super.solve(given, h);
  }
  
  /**
   * x_n+1 = x_n + h*x'
   * @param current Values of the input vector at the current time step.
   * @param f A function that is the derivative of the vector.
   * @param h Step size.
   * @return The values of the input vector at the next time step in Euler's 
   * method.
   */
  @Override
  protected Vector next(Vector current, VectorFunction f, double h) {
    // f(x) = x'
    Vector tmp = f.evaluate(current); // tmp = f(x)
    tmp = tmp.mult(h);                // tmp = h * f(x)
    return current.add(tmp);          // return x_n + h * f(x)
  }
  
  
  @Override
  protected void log(Vector result) {
    log.add(result);
  }
  
  @Override
  public String toString() {
    String str = "";
    
    for (int i = 0; i < log.size(); i++) {
      str += String.format("x(%.5f) = ", h * i);
      str += log.get(i);
      str += "\n";
    }
    
    return str.trim();
  }


}
