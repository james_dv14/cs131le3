/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131le3.core;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface VectorFunction {
  
  /**
   * 
   * @param v An input vector.
   * @return A new vector that is the result of applying the overriding function
   * to the input.
   */
  public Vector evaluate(Vector v);

}
