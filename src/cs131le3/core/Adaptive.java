/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131le3.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Adaptive {
  HashMap<Double, String> log;
  
  /**
   * 
   * @param given Common given for vectors: the initial values, the vector 
   * function, and the starting and ending values for the independent variable.
   * @param m Numerical method.
   * @param itmax Maximum number of iterations.
   * @param e Tolerance.
   */
  public void solve(VectorGiven given, NumericalMethod m, int itmax, double e) {
    double max_diff;
    double h = given.b - given.a;
    
    log = new HashMap();
    for (int i = 0; i < itmax; i++) {
      max_diff = m.solve(given, h);
      log.put(h, m.toString());
      if (max_diff < e) break;
      h /= 2;
    }
  }
  
  @Override
  public String toString() {
    String str = "";
    Object[] sizes = log.keySet().toArray();
    Arrays.sort(sizes, Collections.reverseOrder());
    
    for (Object _size : sizes) {
      Double size = (Double)_size;
      str += String.format("--- h = %.5f ---\n", size);
      str += log.get(size);
      str += "\n\n";
    }
    
    return str.trim();
  }

}
